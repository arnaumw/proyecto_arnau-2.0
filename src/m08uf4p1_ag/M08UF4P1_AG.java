/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m08uf4p1_ag;

import java.util.Scanner;

public class M08UF4P1_AG {

    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in); //Define el teclado
        System.out.println("Buenos días, me llamo Arnau!");
        System.out.print("Pulsa ENTER per terminar... ");
        teclado.nextLine();
    }
    
    public void miMetodo1(int argumento1){
        return;
    }   
    
    public void miMetodo2(int argumento2){
        return;
    } 
    
    public void miMetodo3(int argumento3){
        return;
    } 
    
    public int fansatma(int numero1, int numero2, int numero3){
        numero1 = 2;
        numero2 = 3;
        numero3 = 0;
        numero3 = numero1 + numero2;
        return (numero3);
       
    } 
}
